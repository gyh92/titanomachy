﻿using UnityEngine;
using System.Collections;

public class rockSpawner : MonoBehaviour {
//	public Transform spawnPointOne1;
//	public Transform spawnPointOne2;
//	public Transform spawnPointOne3;
//	public Transform spawnPointOne4;
//	public Transform spawnPointOne5;
//
//	public Transform spawnPointTwo1;
//	public Transform spawnPointTwo2;
//	public Transform spawnPointTwo3;
//	public Transform spawnPointTwo4;
//	public Transform spawnPointTwo5;

	public GameObject spawnPointOne;
	public GameObject spawnPointThree;

	public GameObject rockPrefab;

	private Transform[] spawnPtOne;
	private Transform[] spawnPtThree;



	// Use this for initialization
	void Start () {
		spawnPtOne = spawnPointOne.GetComponentsInChildren<Transform> ();
		spawnPtThree = spawnPointThree.GetComponentsInChildren<Transform> ();
		spawnOne ();
		spawnThree ();
	}
	
	// Update is called once per frame
	void Update () {
//		for (int i = 0; i < spawnPtOne.Length; i++) {
//			Debug.Log (spawnPtOne[i]);
//		}
		if (Input.GetKeyDown (KeyCode.Backslash)) {

			spawnOne ();
			spawnThree ();
		}
	}

	public void spawnOne(){

		for (int i = 0; i < spawnPtOne.Length; i++) {
			GameObject rock = (GameObject)Instantiate (
				                 rockPrefab,
								 spawnPtOne[i].position,
				Quaternion.Euler(0, 0, 0));

			//rock.transform.localScale = new Vector3(2f, 2f, 2f);
		}

	}

	public void spawnThree(){

		for (int i = 0; i < spawnPtThree.Length; i++) {
			GameObject rock = (GameObject)Instantiate (
				rockPrefab,
				spawnPtThree[i].position,
				Quaternion.Euler(0, 0, 0));

			//rock.transform.localScale = new Vector3(2f, 2f, 2f);
		}

	}

}
