﻿using UnityEngine;
using System.Collections;

public class enemySpawnerLocal : MonoBehaviour {
	

	public GameObject spawnPointTwo;
	public GameObject spawnPointFive;
	public GameObject spawnPointOne;
	public GameObject spawnPointThree;
	public GameObject spawnPointFour;

	public GameObject enemyPrefab;

	private Transform[] spawnPtTwo;
	private Transform[] spawnPtFive;
	private Transform[] spawnPtOne;
	private Transform[] spawnPtThree;
	private Transform[] spawnPtFour;


	// Use this for initialization
	void Start () {
		spawnPtTwo = spawnPointTwo.GetComponentsInChildren<Transform> ();
		spawnPtThree = spawnPointThree.GetComponentsInChildren<Transform> ();
		spawnPtFour = spawnPointFour.GetComponentsInChildren<Transform> ();
		spawnPtFive = spawnPointFive.GetComponentsInChildren<Transform> ();
		spawnPtOne = spawnPointOne.GetComponentsInChildren<Transform> ();

	}

	// Update is called once per frame
	void Update () {
		//		for (int i = 0; i < spawnPtOne.Length; i++) {
		//			Debug.Log (spawnPtOne[i]);
		//		}
		if (Input.GetKeyDown (KeyCode.Equals)) {

			spawnTwo ();
			spawnFive ();
			spawnOne ();
			spawnThree ();
			spawnFour ();

		}
	}

	public void spawnTwo(){

		for (int i = 0; i < spawnPtTwo.Length; i++) {
			GameObject enemy = (GameObject)Instantiate (
				enemyPrefab,
				spawnPtTwo[i].position,
				Quaternion.Euler(0, 0, 0));
			
		}

	}

	public void spawnFour(){

		for (int i = 0; i < spawnPtFour.Length; i++) {
			GameObject enemy = (GameObject)Instantiate (
				enemyPrefab,
				spawnPtFour[i].position,
				Quaternion.Euler(0, 0, 0));

		}

	}

	public void spawnThree(){
		for (int i = 0; i < spawnPtThree.Length; i++) {
			GameObject enemy = (GameObject)Instantiate (
				enemyPrefab,
				spawnPtThree[i].position,
				Quaternion.Euler(0, 0, 0));

		}
	}

	public void spawnOne(){

		for (int i = 0; i < spawnPtOne.Length; i++) {
			GameObject enemy = (GameObject)Instantiate (
				enemyPrefab,
				spawnPtOne[i].position,
				Quaternion.Euler(0, 0, 0));

		}

	}

	public void spawnFive(){

		for (int i = 0; i < spawnPtFive.Length; i++) {
			GameObject enemy = (GameObject)Instantiate (
				enemyPrefab,
				spawnPtFive[i].position,
				Quaternion.Euler(0, 0, 0));
			//rock.transform.localScale = new Vector3(2f, 2f, 2f);

		}

	}

}
