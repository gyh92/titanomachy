﻿using UnityEngine;
using System.Collections;

public class plDamege : MonoBehaviour {

	public int damageValue;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.tag == "player") {
			collision.gameObject.GetComponent<Renderer> ().material.color = Color.red;

			GameObject hit = collision.gameObject;
			HealthLocal health = hit.GetComponent<HealthLocal> ();
			if (health != null) {
				health.TakeDamage (damageValue);
			}
		}

	}
	void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.gameObject.tag == "player") {
			collision.gameObject.GetComponent<Renderer> ().material.color = Color.white;

		}

	}
}
