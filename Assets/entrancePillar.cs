﻿using UnityEngine;
using System.Collections;

public class entrancePillar : MonoBehaviour {

	private SpriteRenderer sr1;

	public Sprite sr1On;

	public Sprite sr1Off;


	// Use this for initialization
	void Start () {
		sr1 = gameObject.GetComponent<SpriteRenderer>();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.tag == "spell") {
			sr1.sprite = sr1On;
		}
	
	}
}
