﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SealHealth : MonoBehaviour {
	public int maxHealth;
	public Canvas winScreen;
	public float shootFrequency;
	public float shootSpeed;
	private SpriteRenderer sr;

	public GameObject enemyPrefab;
	public Transform spawn1;
	public Transform spawn2;
	public Transform spawn3;
	public Transform spawn4;

	private float timer;

	private int currentHealth;

	private HeadBehaviour head;
	private Camera camera;
	public GameObject laserPrefab;

	public float leftPoint;
	public float rightPoint;

	private Vector3 pointTo;
	private Vector3 pointBack;
	private float timerS;

	public float triggerBeam;
	private GameObject headObj;

	private float shakeTimer;
	private float inShakeTimer;
	private float amplitude;
	private Vector3 headIni;

	void Start () {
		currentHealth = maxHealth;
		sr = GetComponent<SpriteRenderer>();
		timer = 0f;
		head = GameObject.Find ("Head").GetComponent<HeadBehaviour> ();
		camera = GameObject.Find ("Main Camera").GetComponent<Camera> ();
		pointTo = new Vector3 (leftPoint, -1f, 0f);
		pointBack = new Vector3 (rightPoint, -1f, 0f);
		timerS = 0f;

		triggerBeam = -10f;
		headObj = GameObject.Find ("Head");
		headIni = new Vector3(0, -3.200001f, 11.91029f);
		amplitude = 0.1f;


	}
	
	// Update is called once per frame
	void Update () {
//		Debug.Log (currentHealth + " seal");
		if (currentHealth <= 0 && currentHealth >= -100) {
			//winScreen.gameObject.SetActive (true);
			//head.MoveUp();

			Application.LoadLevel ("WinningScreen");

//
//
//			IEnumerator Coroutine1 = wait ();
//			StartCoroutine (Coroutine1);
//
//			//camera.GetComponent<movingCamara> ().ShakeCamera (0.6f);
//
//		
//			IEnumerator Coroutine = stopDelay ();
//			StartCoroutine (Coroutine);
//
//			GameObject enemy = (GameObject)Instantiate (
//				enemyPrefab,
//				spawn1.transform.position,
//				Quaternion.Euler (new Vector3 (0f, 0f, 0f)));
//			GameObject enemy1 = (GameObject)Instantiate (
//				enemyPrefab,
//				spawn2.transform.position,
//				Quaternion.Euler (new Vector3 (0f, 0f, 0f)));
//			GameObject enemy2 = (GameObject)Instantiate (
//				enemyPrefab,
//				spawn3.transform.position,
//				Quaternion.Euler (new Vector3 (0f, 0f, 0f)));
//			GameObject enemy3 = (GameObject)Instantiate (
//				enemyPrefab,
//				spawn4.transform.position,
//				Quaternion.Euler (new Vector3 (0f, 0f, 0f)));

		}

		if (sr.color != Color.white) {
			timer += Time.deltaTime;
			if (timer >= 0.1f) {
				sr.color = Color.white;
			}
		}
		if (sr.color == Color.white) {
			timer = 0f;
		}
		//if(headsr)

		//ShootLight (shootFrequency, shootSpeed);

		if (triggerBeam >= 0f){
			//ShootLight(shootFrequency, shootSpeed);
			if (pointTo.x >= leftPoint && pointTo.x < rightPoint){
				Debug.Log ("SHOOOOT " + (pointTo.x >= leftPoint && pointTo.x < rightPoint) + timerS + " " + pointTo.x);
				timerS += Time.deltaTime;

				if (timerS >= shootFrequency) {
					Debug.Log ("SHOOOOOOOOOOOOOOOOOOOOT");
					Vector3 dir = pointTo.normalized;
					GameObject laser = (GameObject)Instantiate (
						laserPrefab,
						gameObject.transform.position,
						Quaternion.Euler (new Vector3 (0f, 0f, 0f)));
					laser.GetComponent<Rigidbody2D> ().velocity = dir * shootSpeed;
					Destroy (laser, 2.6f);
					timerS = 0f;
				}

				pointTo.x += 0.03f;

			}

			if (pointTo.x >= rightPoint) {
				timerS += Time.deltaTime;

				if (timerS >= shootFrequency) {
					Vector3 dir = pointBack.normalized;
					GameObject laser = (GameObject)Instantiate (
						laserPrefab,
						gameObject.transform.position,
						Quaternion.Euler (new Vector3 (0f, 0f, 0f)));
					laser.GetComponent<Rigidbody2D> ().velocity = dir * shootSpeed;
					Destroy (laser, 2.6f);
					timerS = 0f;
				}
				pointBack.x -= 0.03f;

			}
		}

//		if (Input.GetKeyDown (KeyCode.Q)) {
//			Reset ();
//			ShootBeam (20f, 0.2f, 10f);
//
//		}
//	

		if (inShakeTimer > 0f) {
			inShakeTimer -= Time.deltaTime;
			float offset = Mathf.Sin(inShakeTimer * 10f * Mathf.PI) * amplitude;
			headObj.transform.position = headObj.transform.position + new Vector3(offset, 0f, 0f);
		}
	}

	public void TakeDemage(int amount){
		currentHealth -= amount;
		inShakeTimer = 0.5f;
		headObj.transform.position = headIni;

	}

	IEnumerator stopDelay(){
		yield return new WaitForSeconds (7f);
		StopAction();
	}
	private void StopAction(){
		camera.GetComponent<movingCamara> ().ShakeCamera (-10f);
		currentHealth = 10;

	}



	IEnumerator wait(){
		yield return new WaitForSeconds (2f);
		//ShootLight (shootFrequency, shootSpeed);
	}




	public void Reset(){
		pointTo = new Vector3 (leftPoint, -1f, 0f);
		pointBack = new Vector3 (rightPoint, -1f, 0f);
		timerS = 0f;
		//triggerBeam = -20f;
//		shootFrequency = sf;
//		shootSpeed = ss;
		Debug.Log ("RESETED");
		//ShootLight (shootFrequency, shootSpeed);
	}

	public void ShootBeam(float shootValue, float sf, float ss){
		shootFrequency = sf;
		shootSpeed = ss;
		triggerBeam = shootValue;
		Debug.Log (triggerBeam + " triggerbeam");
	}


	public void ShootLight(float shootFrequency, float shootSpeed){
		

		if (pointTo.x >= leftPoint && pointTo.x < rightPoint){
//			Debug.Log ("SHOOOOT " + (pointTo.x >= leftPoint && pointTo.x < rightPoint));
			timerS += Time.deltaTime;
		
			if (timerS >= shootFrequency) {
				Debug.Log ("SHOOOOOOOOOOOOOOOOOOOOT");
				Vector3 dir = pointTo.normalized;
				GameObject laser = (GameObject)Instantiate (
					                  laserPrefab,
					                  gameObject.transform.position,
					                  Quaternion.Euler (new Vector3 (0f, 0f, 0f)));
				laser.GetComponent<Rigidbody2D> ().velocity = dir * shootSpeed;
				Destroy (laser, 2.6f);
				timerS = 0f;
			}

			pointTo.x += 0.03f;

		}

		if (pointTo.x >= rightPoint) {
			timerS += Time.deltaTime;

			if (timerS >= shootFrequency) {
				Vector3 dir = pointBack.normalized;
				GameObject laser = (GameObject)Instantiate (
					                   laserPrefab,
					                   gameObject.transform.position,
					                   Quaternion.Euler (new Vector3 (0f, 0f, 0f)));
				laser.GetComponent<Rigidbody2D> ().velocity = dir * shootSpeed;
				Destroy (laser, 2.6f);
				timerS = 0f;
			}
			pointBack.x -= 0.03f;

		}
	}


	public void Beam(float timer){
		if (timer >= 0f) {

			Debug.Log ("Timer ");

			timer -= Time.deltaTime;
		}

	}



}
