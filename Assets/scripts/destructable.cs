﻿using UnityEngine;
using System.Collections;

public class destructable : MonoBehaviour {
	public float maxHealth;
	private float health;
	public Sprite broken;
	public Sprite broken2;
	// Use this for initialization
	void Start () {
		health = maxHealth;

	}
	
	// Update is called once per frame
	void Update () {
		if (health < maxHealth) {
			GetComponent<SpriteRenderer> ().sprite = broken;

		}
		if (health < 50) {
			GetComponent<SpriteRenderer> ().sprite = broken2;

		}
		if (health < 0) {
			Destroy (gameObject);
		}


	}

	public void TakeDamage(int amount)
	{
//		Debug.Log("Enemey take damage " + health);
		health -= amount;

		if (health <= 0)
		{
			Destroy (gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D collision)
	{

		if (collision.gameObject.tag == "axe") {
			health -= 10;
		}
	}
}
