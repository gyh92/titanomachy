﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class localPlayerControl : MonoBehaviour {

	public int facing_state;
	private FacingStateLocal fs;

	private int facing_temp;

	public float speed;
	public float spellSpeed;
	public GameObject spell;
	public Transform spellSpawn;
	public float spellSpan;

	public Sprite player_left; //0
	public Sprite player_right;//1
	public Sprite player_up;   //2	
	public Sprite player_down; //3
	private SpriteRenderer sr;

	public KeyCode fire_key;
	public KeyCode fire_button;
	public KeyCode alt_fire_button;
	public KeyCode attack_key;
	public KeyCode attack_button;
	public string LeftTrigger;
	public string RightTrigger;
	public string StickX;
	public string StickY;
	public string StickRX;
	public string StickRY;
//	public KeyCode UpKey;
//	public KeyCode DownKey;
//	public KeyCode LeftKey;
//	public KeyCode RightKey;
	public string Horizontal;
	public string Vertical;


	private Vector2 moving_velocity;
	private Vector3 move = Vector3.zero;
	private GameObject[] players;

	public GameObject AxePrefab;
	public Transform AxeLeft;
	public Transform AxeRight;
	public Transform AxeUp;
	public Transform AxeDown;
	public float AxeSpan;

	private float shoot_timer;
	private float lastMoveX;
	private float lastMoveY;
	private float tDirX;
	private float tDirY;
	private float shootX;
	private float shootY;


	// Use this for initialization
	void Start () {
		facing_state = 0;
		sr = GetComponent<SpriteRenderer> ();
		facing_temp = facing_state;
		fs = GetComponent<FacingStateLocal> ();

		shoot_timer = 3f;
		lastMoveX = 0f;
		lastMoveY = 0f;
		shootX = 0f;
		shootY = 0f;
//		AxeUp = GameObject.Find ("AxeUp");
//		AxeDown = GameObject.Find ("AxeDown");
//		AxeLeft = GameObject.Find ("AxeLeft");
//		AxeRight = GameObject.Find ("AxeRight");


	}

	// Update is called once per frame
	void Update () {

		//Network setting
	
		//GUIControl.DisplayGUI ();

		//Player control


		float keyX = Input.GetAxis (Horizontal);
		float keyY = Input.GetAxis (Vertical);

		//Joystick
		float dirX = 0f, dirY = 0f, magnitude = 0f;
		dirX = Input.GetAxis (StickX);
		dirY = -Input.GetAxis (StickY);
		magnitude = new Vector3 (dirX, dirY, 0f).magnitude;

		//Joystick Right
		float dirRX = 0f, dirRY = 0f, magnitudeRight= 0f;
		dirRX = Input.GetAxis (StickRX);
		dirRY = -Input.GetAxis (StickRY);
		magnitudeRight = new Vector3 (dirRX, dirRY, 0f).magnitude;


		if (Mathf.Abs (keyX) > 0.3f || Mathf.Abs (keyY) > 0.3f) {
			move = new Vector3 (keyX, keyY, 0);
			transform.position += move * speed * Time.deltaTime;
//			Debug.Log ("Moving");
		}


//		if (Mathf.Abs (dirX) >= 0.8f || Mathf.Abs (dirY) >= 0.8f) {
//			Debug.Log ("dirX " + dirX + "dirY " + dirY);
//			move = new Vector3 (dirX, dirY);
//			transform.position += move * speed * Time.deltaTime;
//
//		}

		if (magnitude > 0.8f) {
			//			Debug.Log ("dirX " + dirX + "dirY " + dirY);
			move = new Vector3 (dirX, dirY);
			transform.position += move * speed * Time.deltaTime;

		}

//		if (Mathf.Abs (dirX) < 0.5f || Mathf.Abs (dirY) < 0.5f) {
//			//			Debug.Log ("dirX " + dirX + "dirY " + dirY);
//			//move = new Vector3 (dirX, dirY);
//			//transform.position += move * speed * Time.deltaTime;
//
//		}
//			
		//Move camera with the player
//		float x = this.transform.position.x;
//		float y = this.transform.position.y;
//		GameObject camera = GameObject.Find ("Main Camera");
//		camera.transform.position = new Vector3 (x, y, camera.transform.position.z);
//


		if (keyX < -0.3f || dirX < -0.3f)
		{
			facing_state = 0;
			fs.facing_state = 0;

			//lastMoveX = -1f;
		}
		if (keyX > 0.3f || dirX > 0.3f)
		{
			facing_state = 1;
			fs.facing_state = 1;
			//lastMoveX = 1f;
		}
		if (keyY > 0.3f || dirY > 0.3f)
		{
			facing_state = 2;
			fs.facing_state = 2;
			//lastMoveY = 1f;
		}
		if (keyY < -0.3f || dirY < -0.3f)
		{
			facing_state = 3;
			fs.facing_state = 3;
			//lastMoveY = -1f;
		}

		if (Mathf.Abs (dirX) < 0.5f) {
			tDirX = lastMoveX;
		}

		if (Mathf.Abs (dirX) > 0.5f) {
			tDirX = dirX;
			lastMoveX = dirX;
		}
		if (Mathf.Abs (dirY) < 0.5f) {
			tDirY = lastMoveY;
		}
		if (Mathf.Abs (dirY) > 0.5f) {
			tDirY = dirY;
			lastMoveY = dirY;
		}

		if (lastMoveX < -0.5f) {
			shootX = -1f;
		}
		if (lastMoveX > 0.5f) {
			shootX = 1f;
		}
		if (lastMoveX >= -0.5f && lastMoveX <= 0.5f) {
			shootX = 0f;
		}
		if (lastMoveY < -0.5f) {
			shootY = -1f;
		}
		if (lastMoveY > 0.5f) {
			shootY = 1f;
		}
		if (lastMoveY >= -0.5f && lastMoveY <= 0.5f) {
			shootY = 0f;
		}
		if (facing_state != facing_temp) {
			ChangeSprite (facing_state);

		}

		Debug.Log (lastMoveX + " shootX " + shootX + " " + lastMoveY + " shootY " + shootY );

		Vector3 shooting_dir = new Vector3 (keyX, keyY, 0).normalized;
		Vector3 shooting_dir_alt = new Vector3 (dirX, dirY, 0).normalized;

		//Debug.Log ("Dir X " + dirX + " Dir Y " + dirY + " shooting_dir_alt " + shooting_dir_alt + " " +  new Vector3 (dirX, dirY, 0).magnitude);

		shoot_timer += Time.deltaTime;
		//Fire
		if (Input.GetKeyDown (fire_key) && shoot_timer >= 1f) 
		{
			Fire (shooting_dir);
			shoot_timer = 0f;
		}

		//Debug.Log (magnitude + " X" + lastMoveX + " X" + dirX + " Y" + lastMoveY + " Y" + dirY);

		if (Input.GetKeyDown (fire_button) && shoot_timer >= 0.5f) {


//			if (magnitudeRight >= 0.3f) {
//				Fire (shooting_dir_alt);
//				shoot_timer = 0f;
//			} 
			if (magnitude >= 0.3f) {
				Fire (shooting_dir_alt);
				shoot_timer = 0f;
			} else {
				if (facing_state == 0) {
					Fire (new Vector3(-1f, 0f, 0f).normalized);
					shoot_timer = 0f;
				}
				if (facing_state == 1) {
					Fire (new Vector3(1f, 0f, 0f).normalized);
					shoot_timer = 0f;
				}
				if (facing_state == 2) {
					Fire (new Vector3(0f, 1f, 0f).normalized);
					shoot_timer = 0f;
				}
				if (facing_state == 3) {
					Fire (new Vector3(0f, -1f, 0f).normalized);
					shoot_timer = 0f;
				}
			}


		}
		if (Input.GetKeyDown (attack_key) || Input.GetKeyDown (attack_button)) {
			Attack (facing_state);
		}

//		if (AxeUp.active) {
//			AxeUp.SetActive (false);
//		}
//		if (AxeDown.active) {
//			AxeDown.SetActive (false);
//		}
//		if (AxeLeft.active) {
//			AxeDown.SetActive (false);
//		}
//		if (AxeRight.active) {
//			AxeRight.SetActive (false);
//		}


//		Debug.Log (facing_temp + " " + facing_state);
		facing_temp = facing_state;

	}
		
	void ChangeSprite(int dir){
		//Debug.Log ("dir" + dir);
		switch(dir){

		case(0):
			GetComponent<SpriteRenderer>().sprite = player_left;
			//Debug.Log ("Cmd facing 0");
			break;
		case(1):
			GetComponent<SpriteRenderer>().sprite= player_right;
//			Debug.Log ("Cmd facing 1");
			break;
		case(2):
			GetComponent<SpriteRenderer>().sprite = player_up;
			//Debug.Log ("Cmd facing 2");
			break;
		case(3):
			GetComponent<SpriteRenderer>().sprite = player_down;
			//Debug.Log ("Cmd facing 3");
			break;
		}
	}

	//Prevent rigidbody pushing
	void OnCollisionEnter2D(Collision2D coll) 
	{
		// If the Collider2D component is enabled on the object we collided with
		if (coll.gameObject.tag == "player")
		{
			// Disables the Collider2D component
			GetComponent<Rigidbody2D>().isKinematic = true;
			//Debug.Log (GetComponent<Rigidbody2D>().isKinematic);
		}
		if (coll.gameObject.tag == "spell")
		{
			gameObject.GetComponent<Renderer> ().material.color = Color.red;
		}

	}

	void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.tag == "chest") {
			//GameObject.Find ("InventoryControl").GetComponent<InventoryList> ().AddItem (coll.gameObject);
			//Destroy (coll.gameObject);
			coll.GetComponent<ChestBehaviour>().Open();
			//GameObject.Find ("InventoryControl").GetComponent<InventoryList> ().AddItem (coll.gameObject);
		}

//		if (coll.gameObject.tag == "item") {
//			GameObject.Find ("InventoryControl").GetComponent<InventoryList> ().AddItem (coll.gameObject);
//			Destroy (coll.gameObject);
//		}
	}

	void OnCollisionExit2D(Collision2D coll){
		if (coll.gameObject.tag == "player")
		{
			// Disables the Collider2D component
			GetComponent<Rigidbody2D>().isKinematic = false;
			//Debug.Log (GetComponent<Rigidbody2D>().isKinematic);

		}

		gameObject.GetComponent<Renderer> ().material.color = Color.white;

	}


	void Fire(Vector3 dir)
	{
		// Create the Bullet from the Bullet Prefab
		GameObject bullet = (GameObject)Instantiate (
			spell,
			spellSpawn.position,
			spellSpawn.rotation);

		//		Debug.Log (bullet.transform.position + " " + bullet.transform.rotation);

		// Add velocity to the bullet
		bullet.GetComponent<Rigidbody2D> ().velocity = dir * spellSpeed;

		// Spawn the bullet on the Clients
		//NetworkServer.Spawn(bullet);

		//Debug.Log (bullet.GetComponent<Rigidbody2D> ().velocity);
		Destroy(bullet, spellSpan);
	}

	void Attack(int dir){
		if (dir == 0) {
			GameObject axe = (GameObject)Instantiate (
				AxePrefab,
				AxeLeft.position,
				AxeLeft.rotation);
			Destroy (axe, AxeSpan);
		}
		if (dir == 1) {
			GameObject axe = (GameObject)Instantiate (
				AxePrefab,
				AxeRight.position,
				AxeRight.rotation);
			Destroy (axe, AxeSpan);


		}
		if (dir == 2) {
			GameObject axe = (GameObject)Instantiate (
				AxePrefab,
				AxeUp.position,
				AxeUp.rotation);
			Destroy (axe, AxeSpan);

		}
		if (dir == 3) {
			GameObject axe = (GameObject)Instantiate (
				AxePrefab,
				AxeDown.position,
				AxeDown.rotation);
			Destroy (axe, AxeSpan);

		}


	}


}


