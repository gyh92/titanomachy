﻿using UnityEngine;
using System.Collections;

public class InventoryList : MonoBehaviour {

	public int InventoryLength;
	public GameObject[] Inventory;
	public int index;


	// Use this for initialization
	void Start () {
		Inventory = new GameObject[InventoryLength];
		index = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
		

	public void AddItem(GameObject item){
		if(index < InventoryLength){
			Inventory [index] = item;
			Debug.Log ("Item ADDED at "+index + " " + Inventory[index].name);

			gameObject.GetComponent<InventoryControl> ().ChangeSprite(item, index);
			GameObject.Find("GUIControl").GetComponent<GUIControl> ().ChangeSprite(item, index);

			index++;
		}

	}
}
