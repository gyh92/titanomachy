﻿using UnityEngine;
using System.Collections;

public class LaserSpawn : MonoBehaviour {


	public GameObject laserPrefab;
	public float lifeSpan;
	public float minFrequency;
	public float maxFrequency;
	public float minLaserSpeed;
	public float maxLaserSpeed;

	private float timer;
	private float frequency;
	private float laserSpeed;

	// Use this for initialization
	void Start () {
		timer = 0f;

	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		frequency = Random.Range (minFrequency, maxFrequency);

		if (timer >= frequency) {
			GameObject laser = (GameObject)Instantiate (
				laserPrefab,
				transform.position,
				transform.rotation);



			//		Debug.Log (bullet.transform.position + " " + bullet.transform.rotation);

			// Add velocity to the bullet

			laserSpeed = Random.Range (minLaserSpeed, maxLaserSpeed);
			laser.GetComponent<Rigidbody2D> ().velocity = new Vector3(1f, 0f, 0f)* laserSpeed;

			// Spawn the bullet on the Clients
			//NetworkServer.Spawn(bullet);

			//Debug.Log (bullet.GetComponent<Rigidbody2D> ().velocity);
			Destroy(laser, lifeSpan);
			timer = 0f;
		}

	}
}
