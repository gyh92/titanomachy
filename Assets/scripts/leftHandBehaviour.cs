﻿using UnityEngine;
using System.Collections;

public class leftHandBehaviour : MonoBehaviour {

	// Use this for initialization
	public int maxHealth;
	public float moveValue;
	public float maxMove;
	public float handTimer;
	public float patternTimer;
	public float patternCountDown;
	public float shakeLength;
	public float shakeFrequency;
	private float shakeTimer;
	private float inShakeTimer;
	private float amplitude;


	public GameObject one;
	public GameObject two;
	public GameObject three;
	public GameObject four;
	public GameObject five;
	public GameObject puzzleFour;
	public GameObject lasers;


	public Transform displayPos;
	private Vector3 outerSpace;


	private float maxMoveValue;
	private Vector3 iniPos;
	private Vector3 maxPos;
	private Vector3 midPos;
	private float timer;
	private bool showPattern;

	private SpriteRenderer sr;
	private float colorTimer;

	public GameObject theCamera;
	public Camera thecam;

	private int currentHealth;

	void Start () {
		currentHealth = maxHealth;
		maxMoveValue = transform.position.x;
		iniPos = transform.position;
		showPattern = false;
		maxPos = transform.position + new Vector3 (-1f, 0f, 0f) * maxMove;
		midPos = (iniPos + maxPos) / 2;

		sr = GetComponent<SpriteRenderer>();
		colorTimer = 0f;
		timer = 0f;
		shakeTimer = shakeFrequency;
		inShakeTimer = shakeLength;
		//inShakeTimer = shakeTimer;

		puzzleFour = GameObject.Find ("puzzleOne");

		amplitude = 0.1f;
		//camera = GameObject.Find ("Main Camera");
		thecam = theCamera.GetComponent<Camera>();
		outerSpace = new Vector3 (-999f, -999f, -5f);
	}


	// Update is called once per frame
	void Update () {
		float abs = Mathf.Abs (transform.position.x - maxMoveValue);

		//Move to the left
	
		if (currentHealth <= 3 && abs <= maxMove) {
			//puzzleFour.GetComponent<BoxCollider2D> ().enabled = true;
			shakeTimer = 100000f;
			transform.position = transform.position + new Vector3(-1f, 0f, 0f) * moveValue;
			lasers.SetActive (false);

		}
		//Debug.Log ("max location" + maxPos + "current Location" + transform.position + " " + ((int)maxPos.x == (int)transform.position.x));
		Vector3 cam = thecam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, thecam.nearClipPlane));

		if ((int)maxPos.x == (int)transform.position.x) {
			timer += Time.deltaTime;
			Debug.Log ("YYEEESSS" + theCamera.transform.position);


			if (timer >= 0 && timer <= patternCountDown) {
				ShowPattern (5);
//				Debug.Log(theCamera.transform.position);
//				five.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
//				five.transform.position = cam;
//				one.transform.position = outerSpace;
//				two.transform.position = outerSpace;
//				three.transform.position = outerSpace;
//				four.transform.position = outerSpace;
			}
			if (timer >= patternCountDown * 2 &&  timer <= patternCountDown * 3) {
//				Debug.Log(theCamera.transform.position);
//
//				three.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
//
//				three.transform.position = cam;
//				one.transform.position = outerSpace;
//				two.transform.position = outerSpace;
//				four.transform.position = outerSpace;
//				five.transform.position = outerSpace;
				ShowPattern (3);
			}
			if (timer >= patternCountDown * 4 &&  timer <= patternCountDown * 5) {
//				Debug.Log(theCamera.transform.position);
//
				ShowPattern (2);
//				two.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
//
//				two.transform.position = cam;
//				one.transform.position = outerSpace;
//				three.transform.position = outerSpace;
//				four.transform.position = outerSpace;
//				five.transform.position = outerSpace;
			}
			if (timer >= patternCountDown * 6 &&  timer <= patternCountDown * 7) {
				//Debug.Log(theCamera.transform.position);

				ShowPattern (1);
//				one.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
//
//				one.transform.position = cam;
//				two.transform.position = outerSpace;
//				three.transform.position = outerSpace;
//				four.transform.position = outerSpace;
//				five.transform.position = outerSpace;
			}
			if (timer >= patternCountDown * 8 &&  timer <= patternCountDown * 9) {
				//Debug.Log(theCamera.transform.position);

				ShowPattern (4);
//				four.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
//
//				four.transform.position = cam;
//				one.transform.position = outerSpace;
//				two.transform.position = outerSpace;
//				three.transform.position = outerSpace;
//				five.transform.position = outerSpace;
			}
			if (timer >= patternCountDown * 10) {
//				four.transform.position = outerSpace;
//				one.transform.position = outerSpace;
//				two.transform.position = outerSpace;
//				three.transform.position = outerSpace;
//				five.transform.position = outerSpace;

				timer = 0f;
				currentHealth = 100;
				transform.position = midPos;
			}

		}


		if (((int)transform.position.x >= (int)midPos.x) && ((int)transform.position.x <= (int)iniPos.x)) {
			//puzzleFour.GetComponent<BoxCollider2D> ().enabled = false;
			shakeTimer = shakeFrequency;
			transform.position = transform.position + new Vector3(1f, 0f, 0f) * (moveValue/2);
			lasers.SetActive (true);

		}
			
		if (sr.color != Color.white) {
			colorTimer += Time.deltaTime;
			if (colorTimer >= 0.1f) {
				sr.color = Color.white;
			}
		}
		if (sr.color == Color.white) {
			colorTimer = 0f;
		}

//		float amplitude = 0.1f;
//		amplitude -= 0.02f;

		if (inShakeTimer > 0f) {
			inShakeTimer -= Time.deltaTime;
			float offset = Mathf.Sin(inShakeTimer * 10f * Mathf.PI) * amplitude;
			transform.position = transform.position + new Vector3(offset, 0f, 0f);
		}
//		if (Input.GetKeyDown (KeyCode.O)) {
//
//			inShakeTimer = shakeLength;
//		}

		if (shakeTimer < 0f) {
			inShakeTimer = shakeLength;
			shakeTimer = shakeFrequency;
		}
			
		shakeTimer -= Time.deltaTime;


//		Vector3 cam = thecam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, thecam.nearClipPlane));
		//Debug.Log("cam" + cam);




	}

	public void TakeDamage(int amount)
	{
//		Debug.Log("Left take damage " + currentHealth);
		currentHealth -= amount;
		inShakeTimer = 0.5f;



	}

	void ShowPattern(int num){
		//Vector3 cc = new Vector3(theCamera.transform.position.x, theCamera.transform.position.y, -5);

		if (num == 1) {
			GameObject POne = (GameObject)Instantiate (
				                 one,
				displayPos.position,
				                 transform.rotation);
			//num = 0;
			//POne.transform.position =  new Vector3(theCamera.transform.position.x, theCamera.transform.position.y, -5);
			//POne.transform.localScale = new Vector3(2f, 2f, 2f);
			Destroy (POne, patternCountDown);
			//num = 0;
		}
		if (num == 2) {
			GameObject PTwo = (GameObject)Instantiate (
				two,
				displayPos.position,
				transform.rotation);
			//num = 0;
			//PTwo.transform.position =  new Vector3(theCamera.transform.position.x, theCamera.transform.position.y, -5);;

//			PTwo.transform.localScale = new Vector3(2f, 2f, 2f);
			Destroy (PTwo, patternCountDown);

		}
		if (num == 3) {
			GameObject PThree = (GameObject)Instantiate (
				three,
				displayPos.position,
				transform.rotation);
			//num = 0;
			//PThree.transform.position =  new Vector3(theCamera.transform.position.x, theCamera.transform.position.y, -5);

//			PThree.transform.localScale = new Vector3(2f, 2f, 2f);

			Destroy (PThree, patternCountDown);

		}
		if (num == 4) {
			GameObject PFour = (GameObject)Instantiate (
				four,
				displayPos.position,
				transform.rotation);
			//num = 0;
			//PFour.transform.position =  new Vector3(theCamera.transform.position.x, theCamera.transform.position.y, -5);

			//PFour.transform.localScale = new Vector3(2f, 2f, 2f);

			Destroy (PFour, patternCountDown);

		}
		if (num == 5) {
			GameObject PFive = (GameObject)Instantiate (
				five,
				displayPos.position,
				transform.rotation);
			//num = 0;
			//PFive.transform.position =  new Vector3(theCamera.transform.position.x, theCamera.transform.position.y, -5);

			//PFive.transform.localScale = new Vector3(2f, 2f, 2f);

			Destroy (PFive, patternCountDown);

		}

	}

}
