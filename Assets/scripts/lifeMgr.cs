﻿using UnityEngine;
using System.Collections;

public class lifeMgr : MonoBehaviour {

	public GameObject playerMelee;
	public GameObject playerRanged;

	public Transform respawnPosRanged;
	public Transform respawnPosMelee;

	private HealthLocal healthMelee;
	private HealthLocal healthRanged;
	private int healthMeleeVal;
	private int healthRangedVal;
	public GameObject patternMgr;
	private HitPattern hitPatternMgr;


	// Use this for initialization
	void Start () {
		healthMelee = playerMelee.GetComponent<HealthLocal> ();
		healthRanged = playerRanged.GetComponent<HealthLocal> ();
		healthMeleeVal = healthMelee.currentHealth;
		healthRangedVal = healthRanged.currentHealth;
		hitPatternMgr = patternMgr.GetComponent<HitPattern> ();
	}
	
	// Update is called once per frame
	void Update () {
//		Debug.Log (healthMelee.GetHealth() + " " + healthRanged.GetHealth());

		if (healthMelee.GetHealth() <= 0 && healthRanged.GetHealth() <= 0) {
			playerMelee.transform.position = respawnPosMelee.position;
			playerRanged.transform.position = respawnPosRanged.position;
//			healthMelee.Respawn ();
//			healthRanged.Respawn ();
			hitPatternMgr.Reset ();

			Application.LoadLevel ("LosingScreen");

		}
	}
}
