﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InventoryControl : MonoBehaviour {
	
	public Canvas bag;
	public GameObject menu; // Assign in inspector
	public KeyCode bag_button;
	public int bag_size;
	public int weapon_index;
	public int inventory_starting_index;
	//public GameObject[] Inventory;
	//public float InventoryLength;

	private Image[] blocks;

	public Sprite prefab;

	private bool isShowing;
	private InventoryList List;
	private string block;


	void Start(){
		block = "block";
		
		//Inventory = List.Inventory;
//		int i = 0;
//		foreach (Image block in bag) {
//			if (i == List.InventoryLength) {
//				break;
//			}
//			blocks [i] = block;
//		}

		for(int j = 0; j < bag_size; j++){
			blocks = bag.GetComponentsInChildren<Image> ();
		}
	}
	void Update() {
		//Debug.Log ("bag!!!");
		if (Input.GetKeyDown(bag_button)) {
			Debug.Log ("bag!!!");
			isShowing = !isShowing;
			menu.SetActive(isShowing);
		}


	}

	public void ChangeSprite(GameObject item, int index){
		string blockName = block + (index+inventory_starting_index).ToString();
//		Debug.Log (blockName + " block name");
		foreach (Image image in blocks)
		{
			if(image.name == blockName)
				image.sprite = prefab;
		}
	}

}
