﻿using UnityEngine;
using System.Collections;

public class rightHandBehaviour : MonoBehaviour {

	// Use this for initialization
	public int maxHealth;
	public float moveValue;
	public float maxMove;
	public float handTimer;
	public float patternTimer;
	public float shakeLength;
	public float shakeFrequency;
	private float shakeTimer;
	private float inShakeTimer;
	private float amplitude;
	public float interval;

	public GameObject puzzleFour;
	public GameObject lasers;


	public Transform displayPos;
	private Vector3 outerSpace;


	private float maxMoveValue;
	private Vector3 iniPos;
	private Vector3 maxPos;
	private Vector3 midPos;
	private float timer;
	private bool showPattern;

	private SpriteRenderer sr;
	private float colorTimer;

	public GameObject theCamera;
	public Camera thecam;

	private int currentHealth;

	void Start () {
		currentHealth = maxHealth;
		maxMoveValue = transform.position.x;
		iniPos = transform.position;
		showPattern = false;
		maxPos = transform.position + new Vector3 (1f, 0f, 0f) * maxMove;
		midPos = (iniPos + maxPos) / 2;

		sr = GetComponent<SpriteRenderer>();
		colorTimer = 0f;
		timer = 0f;
		shakeTimer = shakeFrequency;
		inShakeTimer = shakeLength;
		//inShakeTimer = shakeTimer;

		puzzleFour = GameObject.Find ("puzzleOne");

		amplitude = 0.1f;
		//camera = GameObject.Find ("Main Camera");

	}


	// Update is called once per frame
	void Update () {
		float abs = Mathf.Abs (transform.position.x - maxMoveValue);

	
		//Move to the left
		if (currentHealth <= 3 && abs <= maxMove) {
			puzzleFour.GetComponent<BoxCollider2D> ().enabled = true;
			shakeTimer = 100000f;
			transform.position = transform.position + new Vector3(1f, 0f, 0f) * moveValue;
			lasers.SetActive (false);
			Debug.Log ("YYEEESSS" +  (int)transform.position.x + (int)maxPos.x);
		}
		//Debug.Log ("max location" + maxPos + "current Location" + transform.position + " " + ((int)maxPos.x == (int)transform.position.x));

		if ((int)maxPos.x == (int)transform.position.x) {
			timer += Time.deltaTime;
			Debug.Log ("YYEEESSS");

			if (timer >= interval) {
				currentHealth = 100;
				transform.position = midPos;
			}

		}


		if (((int)transform.position.x <= (int)midPos.x) && ((int)transform.position.x >= (int)iniPos.x)) {
			puzzleFour.GetComponent<BoxCollider2D> ().enabled = false;
			shakeTimer = shakeFrequency;
			transform.position = transform.position + new Vector3(-1f, 0f, 0f) * (moveValue/2);
			lasers.SetActive (true);
			timer = 0f;

		}

		if (sr.color != Color.white) {
			colorTimer += Time.deltaTime;
			if (colorTimer >= 0.1f) {
				sr.color = Color.white;
			}
		}
		if (sr.color == Color.white) {
			colorTimer = 0f;
		}

		//		float amplitude = 0.1f;
		//		amplitude -= 0.02f;

		if (inShakeTimer > 0f) {
			
			inShakeTimer -= Time.deltaTime;
			float offset = Mathf.Sin(inShakeTimer * 10f * Mathf.PI  * -1f) * amplitude;
			transform.position = transform.position + new Vector3(offset, 0f, 0f);
		}
		//		if (Input.GetKeyDown (KeyCode.O)) {
		//
		//			inShakeTimer = shakeLength;
		//		}

		if (shakeTimer < 0f) {
//			Debug.Log ("Right Hand Shake");
			inShakeTimer = shakeLength;
			shakeTimer = shakeFrequency;
		}

//		Debug.Log ("Right Hand Shake Update");

		shakeTimer -= Time.deltaTime;


		//		Vector3 cam = thecam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, thecam.nearClipPlane));
		//Debug.Log("cam" + cam);




	}

	public void TakeDamage(int amount)
	{
		//		Debug.Log("Left take damage " + currentHealth);
		currentHealth -= amount;
		inShakeTimer = 0.5f;



	}

}
