﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class EnemySpawner : NetworkBehaviour {

	public GameObject enemyPrefab;
	public int numberOfEnemies;
	// Use this for initialization
	public override void OnStartServer()
	{
		for (int i = 0; i < numberOfEnemies; i++) {
			Vector3 spawnPosition = new Vector3 (Random.Range (-10f, 10f), Random.Range (-10f, 10f), 0f);
			Quaternion spawnRotation = Quaternion.Euler (0.0f, 0.0f, 0.0f);
			GameObject enemy = (GameObject)Instantiate (enemyPrefab, spawnPosition, spawnRotation);
			NetworkServer.Spawn (enemy);

		}
	}

}
