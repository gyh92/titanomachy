﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class localMeleePlayerControl : MonoBehaviour {

	public int facing_state;

	private int facing_temp;

	public float speed;
	public float spellSpeed;
	public GameObject spell;
	public Transform spellSpawn;

	public Sprite player_left; //0
	public Sprite player_right;//1
	public Sprite player_up;   //2	
	public Sprite player_down; //3
	private SpriteRenderer sr;

	public KeyCode fire_key;
	public KeyCode fire_button;
	public string LeftTrigger;
	public string RightTrigger;
	public string StickX;
	public string StickY;


	private Vector2 moving_velocity;
	private Vector3 move = Vector3.zero;
	private GameObject[] players;

	// Use this for initialization
	void Start () {
		facing_state = 0;
		sr = GetComponent<SpriteRenderer> ();
		facing_temp = facing_state;
	}

	// Update is called once per frame
	void Update () {

		//Network setting
	
		//GUIControl.DisplayGUI ();

		//Player control

		float keyX = Input.GetAxis ("Horizontal");
		float keyY = Input.GetAxis ("Vertical");

		if (Mathf.Abs (keyX) > 0.3f || Mathf.Abs (keyY) > 0.3f) {
			move = new Vector3 (keyX, keyY, 0);
			transform.position += move * speed * Time.deltaTime;
		}

		//Joystick
		float dirX = 0f, dirY = 0f;
		dirX = Input.GetAxis (StickX);
		dirY = -Input.GetAxis (StickY);
		if (Mathf.Abs (dirX) >= 0.5f || Mathf.Abs (dirY) >= 0.5f) {
			Debug.Log ("dirX " + dirX + "dirY " + dirY);
			move = new Vector3 (dirX, dirY);
			transform.position += move * speed * Time.deltaTime;
		}



		//Move camera with the player
//		float x = this.transform.position.x;
//		float y = this.transform.position.y;
//		GameObject camera = GameObject.Find ("Main Camera");
//		camera.transform.position = new Vector3 (x, y, camera.transform.position.z);


		if (keyX < -0.5f || dirX < -0.5f)
		{
			facing_state = 0;
		}
		else if (keyX > 0.5f || dirX > 0.5f)
		{
			facing_state = 1;
		}
		else if (keyY > 0.5f || dirY > 0.5f)
		{
			facing_state = 2;
		}
		else if (keyY < -0.5f || dirY < -0.5f)
		{
			facing_state = 3;
		}

		if (facing_state != facing_temp) {
			ChangeSprite (facing_state);
			ChangeSprite (facing_state);
		}

		Vector3 shooting_dir = new Vector3 (keyX, keyY, 0).normalized;
		Vector3 shooting_dir_alt = new Vector3 (dirX, dirY, 0).normalized;

		//Fire
		if (Input.GetKeyDown (fire_key)) 
		{
			Fire (shooting_dir);
		}
		if (Input.GetKeyDown (fire_button)) {
			Fire (shooting_dir_alt);
		}
		Debug.Log (facing_temp + " " + facing_state);
		facing_temp = facing_state;

	}


	void ChangeSprite(int dir){
		//Debug.Log ("dir" + dir);
		switch(dir){

		case(0):
			GetComponent<SpriteRenderer>().sprite = player_left;
			break;
		case(1):
			GetComponent<SpriteRenderer>().sprite= player_right;
			break;
		case(2):
			GetComponent<SpriteRenderer>().sprite = player_up;
			break;
		case(3):
			GetComponent<SpriteRenderer>().sprite = player_down;
			break;
		}
	}

//	void OnChangeDirection(int facing_state){
//		switch(facing_state){
//
//		case(0):
//			GetComponent<SpriteRenderer>().sprite = player_left;
//			Debug.Log ("OnChange facing 0");
//			break;
//		case(1):
//			GetComponent<SpriteRenderer>().sprite = player_right;
//			Debug.Log ("OnChange facing 1");
//			break;
//		case(2):
//			GetComponent<SpriteRenderer>().sprite = player_up;
//			Debug.Log ("OnChange facing 2");
//			break;
//		case(3):
//			GetComponent<SpriteRenderer>().sprite = player_down;
//			Debug.Log ("OnChange facing 3");
//			break;
//		}
//	}

	//Prevent rigidbody pushing
	void OnCollisionEnter2D(Collision2D coll) 
	{
		// If the Collider2D component is enabled on the object we collided with
		if (coll.gameObject.tag == "player")
		{
			// Disables the Collider2D component
			GetComponent<Rigidbody2D>().isKinematic = true;
			//Debug.Log (GetComponent<Rigidbody2D>().isKinematic);
		}
		if (coll.gameObject.tag == "spell")
		{
			gameObject.GetComponent<Renderer> ().material.color = Color.red;
		}

	}

	void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.tag == "chest") {
			//GameObject.Find ("InventoryControl").GetComponent<InventoryList> ().AddItem (coll.gameObject);
			//Destroy (coll.gameObject);
			coll.GetComponent<ChestBehaviour>().Open();
			//GameObject.Find ("InventoryControl").GetComponent<InventoryList> ().AddItem (coll.gameObject);
		}

		if (coll.gameObject.tag == "item") {
			GameObject.Find ("InventoryControl").GetComponent<InventoryList> ().AddItem (coll.gameObject);
			Destroy (coll.gameObject);
		}
	}

	void OnCollisionExit2D(Collision2D coll){
		if (coll.gameObject.tag == "player")
		{
			// Disables the Collider2D component
			GetComponent<Rigidbody2D>().isKinematic = false;
			//Debug.Log (GetComponent<Rigidbody2D>().isKinematic);

		}

		gameObject.GetComponent<Renderer> ().material.color = Color.white;

	}


	void Fire(Vector3 dir)
	{
		// Create the Bullet from the Bullet Prefab
		GameObject bullet = (GameObject)Instantiate (
			spell,
			spellSpawn.position,
			spellSpawn.rotation);

		//		Debug.Log (bullet.transform.position + " " + bullet.transform.rotation);

		// Add velocity to the bullet
		bullet.GetComponent<Rigidbody2D> ().velocity = dir * spellSpeed;

		// Spawn the bullet on the Clients
		//NetworkServer.Spawn(bullet);

		//Debug.Log (bullet.GetComponent<Rigidbody2D> ().velocity);
		Destroy(bullet, 2.0f);
	}

}


