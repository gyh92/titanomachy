﻿using UnityEngine;
using System.Collections;

public class spell : MonoBehaviour {

	public Color changeColor;
	public Color sealColor;
	public GameObject laserPrefab;
//
//	public GameObject enemySpawn;
//	public GameObject rockSpawn;
//
	private enemySpawnerLocal es;
	private rockSpawner rs;

	private HitPattern patternMgr;

	void Start(){
		patternMgr = GameObject.Find ("HitPatternMgr").GetComponent<HitPattern> ();
//		es = enemySpawn.GetComponent<enemySpawnerLocal> ();
//		rs = rockSpawn.GetComponent<rockSpawner> ();
	}
	void Update(){

	}

	void OnTriggerEnter2D(Collider2D collision)
	{
//		Debug.Log ("Hit");
		if (collision.gameObject.tag == "enemy") {
			GameObject hit = collision.gameObject;
			hit.GetComponent<SpriteRenderer> ().color = Color.red;
			Debug.Log ("Hit enemy");
		}

		if (collision.gameObject.tag == "laser") {
			GameObject hit = collision.gameObject;
			Destroy (hit);
			Destroy (gameObject);

		}

		if (collision.gameObject.tag == "puzzle") {
			GameObject hit = collision.gameObject;
			//hit.GetComponent<SpriteRenderer> ().color = changeColor;
			Debug.Log ("Hit puzzle");

			if (collision.name == "puzzleOne") {
				patternMgr.addNumber (1);
			}
			if (collision.name == "puzzleTwo") {
				patternMgr.addNumber (2);
			}
			if (collision.name == "puzzleThree") {
				patternMgr.addNumber (3);
			}
			if (collision.name == "puzzleFour") {
				patternMgr.addNumber (4);
//				if (patternMgr.currentDigit == 4) {
//					es.spawnFive ();
//					rs.spawnThree ();
//				}
			}
			if (collision.name == "puzzleFive") {
				patternMgr.addNumber (5);
			}
			if (collision.name == "entrancePillar1") {
				Debug.Log ("Hit entrance pillar 1");
				GameObject entrance = GameObject.Find ("entrance1");
				GameObject entrancePillar = GameObject.Find ("entrancePillar1");
				Vector3 dir = (entrance.transform.position - entrancePillar.transform.position).normalized;
				GameObject laser = (GameObject)Instantiate (
					laserPrefab,
					entrancePillar.transform.position,
					Quaternion.Euler(new Vector3(0f, 0f, 45)));



				//		Debug.Log (bullet.transform.position + " " + bullet.transform.rotation);

				// Add velocity to the bullet

				laser.transform.eulerAngles = dir;
				laser.GetComponent<Rigidbody2D> ().velocity = dir * 10f;
				Destroy (laser, 0.3f);

				Destroy (entrance, 0.3f);
			
			}

			if (collision.name == "entrancePillar2") {
				Debug.Log ("Hit entrance pillar 2");
				GameObject entrance = GameObject.Find ("entrance2");
				GameObject entrancePillar = GameObject.Find ("entrancePillar2");
				Vector3 dir = (entrance.transform.position - entrancePillar.transform.position).normalized;
				GameObject laser = (GameObject)Instantiate (
					laserPrefab,
					entrancePillar.transform.position,
					Quaternion.Euler(new Vector3(0f, 0f, 45)));



				//		Debug.Log (bullet.transform.position + " " + bullet.transform.rotation);

				// Add velocity to the bullet

				laser.transform.eulerAngles = dir;
				laser.GetComponent<Rigidbody2D> ().velocity = dir * 10f;
				Destroy (laser, 0.3f);

				Destroy (entrance, 0.3f);

			}

				
		}
		if (collision.gameObject.tag == "Head") {
			GameObject hit = collision.gameObject;
			//hit.GetComponent<SpriteRenderer> ().color = Color.red;
			//HeadBehaviour head = hit.GetComponent<HeadBehaviour> ();
			//head.TakeDamage (10);
		}
		if (collision.gameObject.tag == "rightHand") {
			Destroy (gameObject);
			//GameObject hit = collision.gameObject;
			//hit.GetComponent<SpriteRenderer> ().color = Color.red;
			//HeadBehaviour head = hit.GetComponent<HeadBehaviour> ();
			//head.TakeDamage (10);
		}
		if (collision.gameObject.tag == "leftHand") {
			Destroy (gameObject);
			//GameObject hit = collision.gameObject;
			//hit.GetComponent<SpriteRenderer> ().color = Color.red;
			//HeadBehaviour head = hit.GetComponent<HeadBehaviour> ();
			//head.TakeDamage (10);
		}
		if (collision.gameObject.tag == "seal") {
			GameObject hit = collision.gameObject;
			hit.GetComponent<SpriteRenderer> ().color = sealColor;
			Debug.Log ("Hit seal");
			//GameObject headObj = GameObject.FindGameObjectWithTag ("Head");
			//Debug.Log (headObj.name + headObj.GetComponent<SpriteRenderer> ().color);
			//headObj.GetComponent<SpriteRenderer> ().color = sealColor;
			SealHealth seal = hit.GetComponent<SealHealth> ();
			seal.TakeDemage (10);
			Destroy (gameObject);

		}
		if (collision.gameObject.tag == "destructible") {
			Destroy (gameObject);
		}
//		if (collision.gameObject.tag == "leftHand") {
//			GameObject hit = collision.gameObject;
//			//hit.GetComponent<SpriteRenderer> ().color = Color.red;
//			Debug.Log ("Hit lefthand");
//			leftHandBehaviour leftHand = hit.GetComponent<leftHandBehaviour> ();
//			leftHand.TakeDamage (10);
//			Destroy (gameObject);
//		}
	}
	void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.gameObject.tag == "destructible") {
			Destroy (gameObject);
		}
		if (collision.gameObject.tag == "enemy") {

			GameObject hit = collision.gameObject;
			hit.GetComponent<SpriteRenderer> ().color = Color.white;
			Destroy (gameObject);
		} 
//		else if (!(collision.gameObject.tag == "player") && !(collision.gameObject.tag == "platform") && !(collision.gameObject.tag == "destructible")) {
//			//Instantiate ();
//			Destroy (gameObject);
//
//		}
		if (collision.gameObject.tag == "Head") {
			GameObject hit = collision.gameObject;
			hit.GetComponent<SpriteRenderer> ().color = Color.white;
			Destroy (gameObject);
		}
//		if (collision.gameObject.tag == "puzzle") {
//			GameObject hit = collision.gameObject;
//			hit.GetComponent<SpriteRenderer> ().color = Color.white;
//			Destroy (gameObject);
//		}

	}


}
