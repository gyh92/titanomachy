﻿using UnityEngine;
using System.Collections;

public class movingCamara : MonoBehaviour {

	public float zoomFactor;
	private GameObject[] players;
	private Camera camera;

//
//	public float shakeLength;
//	public float shakeFrequency;
	private float shakeTimer;
//	private float inShakeTimer;
	public float amplitude;
	//private float zFactor;

	// Use this for initialization
	void Start () {
		camera = gameObject.GetComponent<Camera> ();
		camera.orthographicSize = 8; 
		players = GameObject.FindGameObjectsWithTag("player");

		shakeTimer = 0f;
		//amplitude = 0.1f;
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log ("cam "+ transform.position);
		if (players.Length > 0) {
			//zFactor = zoomFactor;
			float x = (players [0].transform.position.x + players [1].transform.position.x)/2;
			float y = (players [0].transform.position.y + players [1].transform.position.y)/2;

			float distance = (players [0].transform.position - players [1].transform.position).magnitude;

			transform.position = new Vector3 (x, y, -10f);
//			Debug.Log (distance);
			camera.orthographicSize = distance * zoomFactor;

			if (camera.orthographicSize < 8) {
				camera.orthographicSize = 8;
			}

			if (camera.orthographicSize > 32) {
				camera.orthographicSize = 32;
			}
				
			if (shakeTimer >= 0f) {
				Debug.Log ("Shake Camera " + shakeTimer);
				shakeTimer -= Time.deltaTime;
				float offset = Mathf.Sin(shakeTimer * 12f * Mathf.PI) * amplitude;
				transform.position = transform.position + new Vector3(0f, offset, 0f);
			}
			//transform.localScale = new Vector3 (1f, 1f, 1f) * distance * zFactor;
		}

//		if (inShakeTimer > 0f) {
//
//			inShakeTimer -= Time.deltaTime;
//			float offset = Mathf.Sin(inShakeTimer * 10f * Mathf.PI) * amplitude;
//			transform.position = transform.position + new Vector3(0f, offset, 0f);
//		}
//
//		if (shakeTimer < 0f) {
//			inShakeTimer = shakeLength;
//			shakeTimer = shakeFrequency;
//		}
//
//		shakeTimer -= Time.deltaTime;

//		if(Input.GetKey(KeyCode.Q)) // Change From Q to anyother key you want
//		{
//			camera.orthographicSize = camera.orthographicSize + 1*Time.deltaTime;
//			if(camera.orthographicSize > 10)
//			{
//				camera.orthographicSize = 10; // Max size
//			}
//		}
//
//
//		if(Input.GetKey(KeyCode.E)) // Also you can change E to anything
//		{
//			camera.orthographicSize = camera.orthographicSize - 1*Time.deltaTime;
//			if(camera.orthographicSize < 8)
//			{
//				camera.orthographicSize = 8; // Min size 
//			}
//		}
	}

	public void ShakeCamera(float timer){

		Debug.Log ("Shake Camera " + timer);

//		if (timer >= 0f) {
//			Debug.Log ("Shake Camera " + timer);
//			timer -= Time.deltaTime;
//			float offset = Mathf.Sin(timer * 10f * Mathf.PI) * amplitude;
//			transform.position = transform.position + new Vector3(0f, offset, 0f);
//		}

		shakeTimer = timer;

	}


}
