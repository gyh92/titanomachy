﻿using UnityEngine;
using System.Collections;


public class dash : MonoBehaviour {

	public DashState dashState;
	public float dashTimer;
	public float maxDash = 5f;
	public float dashing_speed;
	public float facing_state;
	public float factor;

	public KeyCode dashingKey;
	public KeyCode dashingKeyAlt;
	public KeyCode dashingButton;

	public Vector2 savedVelocity;

	void Update () 
	{
		
		switch (dashState) 
		{
		case DashState.Ready:
			bool isDashKeyDown = (Input.GetKeyDown (dashingKey) || Input.GetKeyDown (dashingKeyAlt) || Input.GetKeyDown (dashingButton) );
			if(isDashKeyDown)
			{
				savedVelocity = gameObject.GetComponent<Rigidbody2D>().velocity;
				Debug.Log ("dashing " + savedVelocity);
				facing_state = GetComponent<FacingStateLocal> ().facing_state;
				if (facing_state == 0f)
					gameObject.GetComponent<Rigidbody2D> ().velocity = (new Vector2 (-1, 0)) * dashing_speed;
				else if (facing_state == 1f)
					gameObject.GetComponent<Rigidbody2D> ().velocity = (new Vector2 (1, 0)) * dashing_speed;
				else if (facing_state == 3f)
					gameObject.GetComponent<Rigidbody2D> ().velocity = (new Vector2 (0, -1)) * dashing_speed;
				else if (facing_state == 4f)
					gameObject.GetComponent<Rigidbody2D> ().velocity = (new Vector2 (0, 1)) * dashing_speed;
				
				Debug.Log ("after " + gameObject.GetComponent<Rigidbody2D>().velocity);
				dashState = DashState.Dashing;
			}
			break;
		case DashState.Dashing:
			dashTimer += Time.deltaTime * factor;
			if(dashTimer >= maxDash)
			{
				dashTimer = maxDash;
				gameObject.GetComponent<Rigidbody2D>().velocity = savedVelocity;
				dashState = DashState.Cooldown;
			}
			break;
		case DashState.Cooldown:
			dashTimer -= Time.deltaTime;
			if(dashTimer <= 0)
			{
				dashTimer = 0;
				dashState = DashState.Ready;
			}
			break;
		}
	}
}

public enum DashState 
{
	Ready,
	Dashing,
	Cooldown
}

