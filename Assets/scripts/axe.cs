﻿using UnityEngine;
using System.Collections;

public class axe : MonoBehaviour {

	public Color changeColor;
	public Color sealColor;
	public Color attackColor;
	private HitPattern patternMgr;

	void Start(){
		patternMgr = GameObject.Find ("HitPatternMgr").GetComponent<HitPattern> ();
	}
	void Update(){

	}

	void OnTriggerEnter2D(Collider2D collision)
	{
		//		Debug.Log ("Hit");
		if (collision.gameObject.tag == "enemy") {
			GameObject hit = collision.gameObject;
			hit.GetComponent<SpriteRenderer> ().color = Color.red;
			//Debug.Log ("Hit enemy");
		}
		if (collision.gameObject.tag == "leftHand") {
			GameObject hit = collision.gameObject;
			//hit.GetComponent<SpriteRenderer> ().color = Color.red;
			Debug.Log ("Hit lefthand");
			hit.GetComponent<SpriteRenderer> ().color = attackColor;	
			leftHandBehaviour leftHand = hit.GetComponent<leftHandBehaviour> ();
			leftHand.TakeDamage (10);
		}
		if (collision.gameObject.tag == "rightHand") {
			GameObject hit = collision.gameObject;
			//hit.GetComponent<SpriteRenderer> ().color = Color.red;
			Debug.Log ("Hit rightHand");
			hit.GetComponent<SpriteRenderer> ().color = attackColor;	
			rightHandBehaviour rightHand = hit.GetComponent<rightHandBehaviour> ();
			rightHand.TakeDamage (10);
		}
		if (collision.gameObject.tag == "Head") {
			GameObject hit = collision.gameObject;
			//hit.GetComponent<SpriteRenderer> ().color = Color.red;
			//HeadBehaviour head = hit.GetComponent<HeadBehaviour> ();
			//head.TakeDamage (10);
		}
		if (collision.gameObject.tag == "seal") {
			GameObject hit = collision.gameObject;
			hit.GetComponent<SpriteRenderer> ().color = sealColor;
			//GameObject headObj = GameObject.FindGameObjectWithTag ("Head");
			//Debug.Log (headObj.name + headObj.GetComponent<SpriteRenderer> ().color);
			//headObj.GetComponent<SpriteRenderer> ().color = sealColor;
			//Debug.Log ("Hit seal");
			SealHealth seal = hit.GetComponent<SealHealth> ();
			seal.TakeDemage (10);

		}

//		if (collision.gameObject.tag == "puzzle") {
//			GameObject hit = collision.gameObject;
//			hit.GetComponent<SpriteRenderer> ().color = changeColor;
//			Debug.Log ("Hit puzzle");
//
//			if (collision.name == "puzzleOne") {
//				patternMgr.addNumber (1);
//			}
//			if (collision.name == "puzzleTwo") {
//				patternMgr.addNumber (2);
//			}
//			if (collision.name == "puzzleThree") {
//				patternMgr.addNumber (3);
//			}
//			if (collision.name == "puzzleFour") {
//				patternMgr.addNumber (4);
//			}
//			if (collision.name == "puzzleFive") {
//				patternMgr.addNumber (5);
//			}
//
//		}
	}

	void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.gameObject.tag == "enemy") {

			GameObject hit = collision.gameObject;
			hit.GetComponent<SpriteRenderer> ().color = Color.white;
			//Destroy (gameObject);
		} else if (!(collision.gameObject.tag == "player") && !(collision.gameObject.tag == "platform")) {
			//Instantiate ();
			//Destroy (gameObject);

		}
		if (collision.gameObject.tag == "Head") {
			GameObject hit = collision.gameObject;
			hit.GetComponent<SpriteRenderer> ().color = Color.white;
		}

	}


}
