﻿using UnityEngine;
using System.Collections;

public class HitPattern : MonoBehaviour {

	private const int SIZE = 5;
	public int[] pattern = new int[SIZE];

	private int[] hitOrder = new int[SIZE];

	private SpriteRenderer sr1;
	private SpriteRenderer sr2;
	private SpriteRenderer sr3;
	private SpriteRenderer sr4;
	private SpriteRenderer sr5;

	public Sprite sr1On;
	public Sprite sr2On;
	public Sprite sr3On;
	public Sprite sr4On;
	public Sprite sr5On;

	public Sprite sr1Off;
	public Sprite sr2Off;
	public Sprite sr3Off;
	public Sprite sr4Off;
	public Sprite sr5Off;

	public Transform pt1;
	public Transform pt2;
	public Transform pt3;
	public Transform pt4;
	public Transform pt5;

	private BoxCollider2D pc1;
	private BoxCollider2D pc2;
	private BoxCollider2D pc3;
	private BoxCollider2D pc4;
	private BoxCollider2D pc5;

	private Camera camera;

	private GameObject headObj;
	private HeadBehaviour head;
	private BodyBehaviour body;

	private float timerP;

	private rockSpawner rSpawner;
	private enemySpawnerLocal eSpawner;
	private SealHealth seal;


	public int currentDigit;
	public GameObject laserPrefab;

	private float laserTrigger;
	private float beamTrigger;

	private float beamTimer;
	private float finalTimer;
	private bool shouldShoot;
	private bool shouldShootFinal;

	// Use this for initialization
	void Start () {
		currentDigit = 0;
		for (int i = 0; i < SIZE; i++) {
			hitOrder [i] = 0;

		}
		Debug.Log (hitOrder[0] + " " + hitOrder[1] + " " + hitOrder[2] + " " + hitOrder[3] + " " + hitOrder[4]);
		sr1 = GameObject.Find ("puzzleOne").GetComponent<SpriteRenderer>();
		sr2 = GameObject.Find ("puzzleTwo").GetComponent<SpriteRenderer>();
		sr3 = GameObject.Find ("puzzleThree").GetComponent<SpriteRenderer>();
		sr4 = GameObject.Find ("puzzleFour").GetComponent<SpriteRenderer>();
		sr5 = GameObject.Find ("puzzleFive").GetComponent<SpriteRenderer>();

		pc1 = GameObject.Find ("puzzleOne").GetComponent<BoxCollider2D> ();
		pc2 = GameObject.Find ("puzzleTwo").GetComponent<BoxCollider2D>();
		pc3 = GameObject.Find ("puzzleThree").GetComponent<BoxCollider2D>();
		pc4 = GameObject.Find ("puzzleFour").GetComponent<BoxCollider2D>();
		pc5 = GameObject.Find ("puzzleFive").GetComponent<BoxCollider2D>();

		head = GameObject.Find ("Head").GetComponent<HeadBehaviour>() ;
		//body = GameObject.Find ("Body").GetComponent<BodyBehaviour>() ;
		headObj = GameObject.Find ("Seal");

		camera = GameObject.Find ("Main Camera").GetComponent<Camera>() ;

		timerP = 0f;

		eSpawner = GameObject.Find ("enemySpawner").GetComponent<enemySpawnerLocal>() ;
		rSpawner = GameObject.Find ("rockSpawner").GetComponent<rockSpawner>() ;

		seal = GameObject.Find ("Seal").GetComponent<SealHealth> (); 

		laserTrigger = 10f;
		beamTrigger = 10f;

		beamTimer = 0f;
		finalTimer = 0f;
		shouldShoot = true;
		shouldShootFinal = true;
	}
	
	// Update is called once per frame
	void Update () {
//		if (Input.GetKeyDown (KeyCode.P)) {
//			addNumber (1);
//			Debug.Log (hitOrder[0] + " " + hitOrder[1] + " " + hitOrder[2] + " " + hitOrder[3] + " " + hitOrder[4]);
//		}
		if (currentDigit >= 5) {
			//timerP += Time.deltaTime;



			pc1.enabled = false;
			pc2.enabled = false;
			pc3.enabled = false;
			pc4.enabled = false;
			pc5.enabled = false;

			seal.Reset ();

			IEnumerator Coroutine = resetDelay ();
			StartCoroutine (Coroutine);
//				pc1.enabled = true;
//				pc2.enabled = true;
//				pc3.enabled = true;
//				pc4.enabled = true;
//				pc5.enabled = true;
//				currentDigit = 0;

		}
			

		if(currentDigit == 3){
			//seal.Reset (0.2f, 10f);
			if(shouldShoot){
				beamTimer += Time.deltaTime;
				if (beamTimer <= 9f) {
					seal.ShootBeam (20f, 0.25f, 10f);
				} else {
					seal.ShootBeam (-20f, 0.25f, 10f);
					seal.Reset ();
					beamTimer = 0f;
					shouldShoot = false;
					//beamTimer = 0f;
				}
			}
//			IEnumerator Coroutine1 = stopBeamDelay ();
//			StartCoroutine (Coroutine1);


			//seal.ShootLight (0.3f, 10f);
		}

		if (currentDigit == 4) {
			seal.ShootBeam (-20f, 0.1f, 10f);
			seal.Reset ();
		}
//		if (currentDigit == 1) {
//			if (hitOrder [0] != pattern [0]) {
//				switch (hitOrder [0]) {
//				case 1:
//					eSpawner.spawnFour ();
//					break;
//
//				case 2:
//					eSpawner.spawnThree ();
//					break;
//			
//				case 3:
//				
//					eSpawner.spawnTwo ();
//					break;
//
//				case 5:
//					eSpawner.spawnOne ();
//					break;
//
//				case 4:
//					eSpawner.spawnFive ();
//					break;
//
//
//				}
//			}
//		}

		if (currentDigit > 1) {
			
			if (hitOrder [0] != pattern [0] || hitOrder [1] != pattern [1]) {

				if (hitOrder [1] != pattern [1]) {
					if (hitOrder [1] == 1) {
						eSpawner.spawnFour ();
					}
					if (hitOrder [1] == 2) {
						eSpawner.spawnThree ();
					}
					if (hitOrder [1] == 3) {
						eSpawner.spawnTwo ();
					}
					if (hitOrder [1] == 5) {
						eSpawner.spawnOne ();
					}
					if (hitOrder [1] == 4) {
						eSpawner.spawnFive ();
					}
				}

				sr1.sprite = sr1Off;
				sr2.sprite = sr2Off;
				sr3.sprite = sr3Off;
				sr4.sprite = sr4Off;
				sr5.sprite = sr5Off;

				shouldShoot = true;

				currentDigit = 0;



			} else if (currentDigit > 2 && hitOrder [2] != pattern [2]) {
				if (hitOrder [2] == 1) {
					eSpawner.spawnFour ();
				}
				if (hitOrder [2] == 2) {
					eSpawner.spawnThree ();
				}
				if (hitOrder [2] == 3) {
					eSpawner.spawnTwo ();
				}
				if (hitOrder [2] == 5) {
					eSpawner.spawnOne ();
				}
				if (hitOrder [2] == 4) {
					eSpawner.spawnFive ();
				}

				sr1.sprite = sr1Off;
				sr2.sprite = sr2Off;
				sr3.sprite = sr3Off;
				sr4.sprite = sr4Off;
				sr5.sprite = sr5Off;

				shouldShoot = true;

				currentDigit = 0;

			} else if (currentDigit > 3 && hitOrder [3] != pattern [3]) {
				if (hitOrder [3] == 1) {
					eSpawner.spawnFour ();
				}
				if (hitOrder [3] == 2) {
					eSpawner.spawnThree ();
				}
				if (hitOrder [3] == 3) {
					eSpawner.spawnTwo ();
				}
				if (hitOrder [3] == 5) {
					eSpawner.spawnOne ();
				}
				if (hitOrder [3] == 4) {
					eSpawner.spawnFive ();
				}

				sr1.sprite = sr1Off;
				sr2.sprite = sr2Off;
				sr3.sprite = sr3Off;
				sr4.sprite = sr4Off;
				sr5.sprite = sr5Off;

				shouldShoot = true;

				currentDigit = 0;

			}
		}


		if ((hitOrder [0] == pattern [0]) && (hitOrder [1] == pattern [1]) && (hitOrder [2] == pattern [2]) && (hitOrder [3] == pattern [3]) && (hitOrder [4] == pattern [4])) {
			Debug.Log("You WIN");
			headObj.GetComponent<BoxCollider2D> ().enabled = true;

			camera.GetComponent<movingCamara> ().ShakeCamera (0.6f);
			if (laserTrigger > 0f) {
				shootLaser (pt1);
				shootLaser (pt2);
				shootLaser (pt3);
				shootLaser (pt4);
				shootLaser (pt5);
			}

//			if (shouldShootFinal) {
//				
//				finalTimer += Time.deltaTime;
//
//				if (finalTimer >= 3f) {
//					laserTrigger = -10f;
//
//					seal.ShootBeam (20f, 0.05f, 10f);
//
//				} else if(finalTimer >= 10f){
//					
//					//beamTrigger = -10f;
//					seal.ShootBeam (-20f, 0.1f, 10f);
//					seal.Reset ();
//					camera.GetComponent<movingCamara> ().ShakeCamera (-10f);
//					head.MoveDown();
//					body.MoveDown ();
//
//					finalTimer = 0f;
//					shouldShootFinal = false;
//					pattern [0] = 0;
//					Reset ();
//
//						//beamTimer = 0f;
//
//				}
//			}
			IEnumerator Coroutine0 = shootDelay ();
			StartCoroutine (Coroutine0);



			IEnumerator Coroutine = stopDelay ();
			StartCoroutine (Coroutine);

			//currentDigit = 0;

		
		}

		if(Input.GetKey(KeyCode.H)){
			headObj.GetComponent<BoxCollider2D> ().enabled = true;
			head.MoveDown();
			//body.MoveDown ();
			camera.GetComponent<movingCamara> ().ShakeCamera (0.6f);

		}
//		for (int i = 0; i < SIZE; i++) {
//			if (HitPattern [i] == pattern [i]) {
//
//			}
//
//		}
//
		if (Input.GetKey (KeyCode.L)) {
			shootLaser (pt1);
			shootLaser (pt2);
			shootLaser (pt3);
			shootLaser (pt4);
			shootLaser (pt5);
		}

		//Debug.Log ("Current Digit " + currentDigit);

	}

	IEnumerator resetDelay(){
		yield return new WaitForSeconds (3f);
		Reset ();
	}
		
	public void Reset(){
//		sr1.color = Color.white;
//		sr2.color = Color.white;
//		sr3.color = Color.white;
//		sr4.color = Color.white;
//		sr5.color = Color.white;
		sr1.sprite = sr1Off;
		sr2.sprite = sr2Off;
		sr3.sprite = sr3Off;
		sr4.sprite = sr4Off;
		sr5.sprite = sr5Off;

		pc1.enabled = true;
		pc2.enabled = true;
		pc3.enabled = true;
		pc4.enabled = true;
		pc5.enabled = true;

		seal.ShootBeam (-20f, 0.1f, 10f);
		seal.Reset ();

		shouldShoot = true;

		currentDigit = 0;
	}

	IEnumerator stopDelay(){
		yield return new WaitForSeconds (14f);
		StopAction();
	}

	private void StopAction(){
		

		head.MoveDown();
//		body.MoveDown ();
		camera.GetComponent<movingCamara> ().ShakeCamera (-10f);
		beamTrigger = -10f;
		seal.ShootBeam (-20f, 0.1f, 10f);
		seal.Reset ();
		shouldShootFinal = false;
		Reset ();
		pattern [0] = 0;

	}

	IEnumerator shootDelay(){
		yield return new WaitForSeconds (3f);
		laserTrigger = -10f;
		shootBeam();
	}

	private void shootBeam(){
		if (shouldShootFinal) {
			seal.ShootBeam (20f, 0.05f, 10f);
		}

	}


	IEnumerator stopBeamDelay(){
		yield return new WaitForSeconds (7f);
		StopBeam();
	}
	private void StopBeam(){
		seal.ShootBeam(-20f, 0.2f, 10f);
	}


	public void addNumber(int digit){

		if (digit == 1) {
			sr1.sprite = sr1On;
		}
		if (digit == 2) {
			sr2.sprite = sr2On;
		}
		if (digit == 3) {
			sr3.sprite = sr3On;
		}
		if (digit == 4) {
			sr4.sprite = sr4On;
		}
		if (digit == 5) {
			sr5.sprite = sr5On;
		}

		if (currentDigit == 0) {
			hitOrder [currentDigit] = digit;
			currentDigit += 1;
			Debug.Log (currentDigit);
		}
		if (currentDigit >= 1 && (digit != hitOrder [currentDigit - 1])) {
			
			hitOrder [currentDigit] = digit;
			currentDigit += 1;
			Debug.Log (currentDigit);


		}

//		if(digit == 4){
//			eSpawner.spawnFive ();
//			rSpawner.spawnThree ();
//		}


		Debug.Log (hitOrder[0] + " " + hitOrder[1] + " " + hitOrder[2] + " " + hitOrder[3] + " " + hitOrder[4]);
			
	}

	public void shootLaser (Transform piller){
		Vector3 dir = (headObj.transform.position - piller.position).normalized;
		GameObject laser = (GameObject)Instantiate (
			laserPrefab,
			piller.position,
			Quaternion.Euler(new Vector3(0f, 0f, 45)));



		//		Debug.Log (bullet.transform.position + " " + bullet.transform.rotation);

		// Add velocity to the bullet

		laser.transform.eulerAngles = dir;
		laser.GetComponent<Rigidbody2D> ().velocity = dir * 10f;
		Destroy (laser, 3f);

	}
}
