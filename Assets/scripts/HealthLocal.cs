﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthLocal : MonoBehaviour {

	public const int maxHealth = 100;

	public Transform respawn;

	public int currentHealth = maxHealth;

	public RectTransform healthBar;

	private Vector3 respawnPosition;
	private float respawnTimer;


	void Start(){
		respawnTimer = 0f;
	}

	void Update(){
		//OnChangeHealth (currentHealth);
//		Debug.Log ("Died " + currentHealth);
		if (currentHealth <= 0)
		{
			gameObject.GetComponent<SpriteRenderer>().color = new Color (1f, 1f, 1f, 0.5f);
			gameObject.GetComponent<localPlayerControl> ().enabled = false;
			IEnumerator Coroutine = respawnDelay ();
			StartCoroutine (Coroutine);
		}

//		if (currentHealth >= maxHealth) {
//			respawnTimer = 0f;
//		}

		healthBar.sizeDelta = new Vector2(currentHealth , healthBar.sizeDelta.y);
	}

	public void TakeDamage(int amount)
	{
		currentHealth -= amount;
		healthBar.sizeDelta = new Vector2(currentHealth , healthBar.sizeDelta.y);
	}

	public int GetHealth(){
		return currentHealth;
	}

	void OnChangeHealth (int currentHealth)
	{
		healthBar.sizeDelta = new Vector2(currentHealth , healthBar.sizeDelta.y);
	}

	IEnumerator respawnDelay(){
		yield return new WaitForSeconds (5f);
		Respawn ();
	}

	public void Respawn()
	{
		
			// move back to zero location
			//transform.position = respawnPosition;
		    gameObject.GetComponent<SpriteRenderer>().color = new Color (1f, 1f, 1f, 1f);

			gameObject.GetComponent<localPlayerControl> ().enabled = true;

			currentHealth = maxHealth;
			healthBar.sizeDelta = new Vector2(currentHealth , healthBar.sizeDelta.y);
//			if (spawnPoints != null && spawnPoints.Length > 0) {
//				spawnPoint = spawnPoints [Random.Range (0, spawnPoints.Length)].transform.position;
//			}
//
//			transform.position = spawnPoint;

	}
}
