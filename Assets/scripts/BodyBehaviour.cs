﻿using UnityEngine;
using System.Collections;

public class BodyBehaviour : MonoBehaviour {


	public int maxHealth;
	public float moveValue;
	public float maxMove;
	public float handTimer;
	public float patternTimer;
	public float patternCountDown;
	public float rotateValue;

	private float maxMoveValue;
	private Vector3 iniPos;
	private Vector3 maxPos;
	private Vector3 midPos;
	private float timer;
	private bool showPattern;

	private int currentHealth;
	// Use this for initialization
	void Start () {
		currentHealth = maxHealth;
		maxMoveValue = transform.position.x;
		iniPos = transform.position;
		timer = 0f;
		showPattern = false;
		maxPos = transform.position + new Vector3 (-1f, 0f, 0f) * maxMove;
		midPos = (iniPos + maxPos) / 2;
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log (transform.position.y + " " + (transform.position.y - maxMoveValue) + " " + maxMove);

	}

	public void MoveDown(){
		Debug.Log ("MOVE BODY");
		float abs = Mathf.Abs (iniPos.y - transform.position.y);
		if (abs <= maxMove) {
			transform.position = transform.position + new Vector3 (0f, -1f, 0f) * moveValue;
		}
	}
	public void MoveUp(){
		float abs = Mathf.Abs (transform.position.y - maxMoveValue);
		if (abs >= 0f) {
			transform.position = transform.position + new Vector3 (0f, 1f, 0f) * moveValue;
		}
	}
}
