﻿using UnityEngine;
using System.Collections;

public class HeadBehaviour : MonoBehaviour {

	public int maxHealth;
	public float moveValue;
	public float maxMove;
	public float secondMove;
	public float handTimer;
	public float patternTimer;
	public float patternCountDown;
	public float rotateValue;

	private float maxMoveValue;
	private Vector3 iniPos;
	private Vector3 maxPos;
	private Vector3 midPos;
	private float timer;
	private bool showPattern;

	private int currentHealth;

	// Use this for initialization
	void Start () {
		currentHealth = maxHealth;
		maxMoveValue = transform.position.x;
		iniPos = transform.position;
		timer = 0f;
		showPattern = false;
		maxPos = transform.position + new Vector3 (0f, -1f, 0f) * maxMove;
		midPos = (iniPos + maxPos) / 2;
	}
	
	// Update is called once per frame
	void Update () {

		//Debug.Log (transform.position.y + " " + (transform.position.y - maxMoveValue) + " " + maxMove);
//		Debug.Log("Head health " + currentHealth);
		if (currentHealth <= 0) {

		}

		if(Input.GetKey(KeyCode.U)){
			MoveUp ();

		}
		if(Input.GetKey(KeyCode.H)){
			MoveDown ();

		}
	}

//	public void TakeDamage(int amount)
//	{
//		//		Debug.Log("Left take damage " + currentHealth);
//		currentHealth -= amount;
//	}

	public void MoveDown(){
		Debug.Log ("MOVE HEAD");
		float abs = Mathf.Abs (iniPos.y - transform.position.y);
		if (abs <= maxMove) {
			transform.position = transform.position + new Vector3 (0f, -1f, 0f) * moveValue;
			transform.eulerAngles += rotateValue * new Vector3(0f, 0f, 1f);
//			Debug.Log (transform.eulerAngles);
		}

	}
	public void MoveUp(){
		float abs = Mathf.Abs (transform.position.y - maxMoveValue);
		if (abs <= secondMove) {
			transform.position = transform.position + new Vector3 (0f, 1f, 0f) * moveValue;
			transform.eulerAngles -= 2.9f * rotateValue * new Vector3(0f, 0f, 1f);
		}
	}

	public void MoveBack(){
		float abs = Mathf.Abs (transform.position.y - iniPos.y);
		if (abs <= (maxMove - secondMove)){
			transform.position = transform.position + new Vector3 (0f, 1f, 0f) * moveValue;
		}
	}

}
