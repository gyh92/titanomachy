﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class enemyHealth : NetworkBehaviour {

	public const int maxHealth = 60;

	[SyncVar(hook="OnChangeEnemyHealth")]
	public int currentHealth = maxHealth;
	public RectTransform healthBar;

	public GameObject enemyItem;

	// Use this for initialization
	void Start () {
		Debug.Log("what " + currentHealth);
	}
	
	// Update is called once per frame
	public void TakeDamage(int amount)
	{
		if (!isServer)
			return;

		currentHealth -= amount;
		if (currentHealth <= 0)
		{
			DropItem ();
			NetworkServer.Destroy (gameObject);
		}
	}

	void OnChangeEnemyHealth (int currentHealth)
	{
		healthBar.sizeDelta = new Vector2(currentHealth , healthBar.sizeDelta.y);

	}

		
//	void OnTriggerEnter2D(Collider2D coll) 
//	{
//		Debug.Log ("CollisionEnter");
//		// If the Collider2D component is enabled on the object we collided with
//		if (coll.gameObject.tag == "spell")
//		{
//			currentHealth -= 10;
//		}
//
//	}


	void DropItem(){
		GameObject enemyItemDrop = (GameObject)Instantiate (
			enemyItem,
			gameObject.transform.position,
			gameObject.transform.rotation);

		NetworkServer.Spawn (enemyItemDrop);
	}
}
