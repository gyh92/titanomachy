﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class GUIControl : MonoBehaviour {

	public static GameObject GUI;
	public Sprite prefab;

	private Image[] blocks;

	private string block;


	// Use this for initialization
	void Start () {
		GUI = GameObject.Find ("GUI");
		Debug.Log ("Loading GUI Control");
		if (GameObject.FindWithTag ("player") == null) {
			GUI.SetActive (false);
		} 

		blocks = GUI.GetComponentsInChildren<Image> ();
		block = "onGUIblock";
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public static void DisplayGUI(){
		GUI.SetActive (true);

	}

	public void ChangeSprite(GameObject item, int index){
		
		index = index % 4 + 1;

//		Debug.Log ("HERE HERE HERE" + index);
		string blockName = block + (index).ToString();

		foreach (Image image in blocks)
		{
			if(image.name == blockName)
				image.sprite = prefab;
		}
	}


//	public GameObject GUI;
//
//	public static GUIControl Instance { get; set; }
//
//	void Awake()
//	{
//		Instance = this;
//	}
//	// Use this for initialization
//	void Start () {
//		Debug.Log ("Loading GUI Control");
//		if (GameObject.FindWithTag ("player") == null) {
//			GUI.SetActive (false);
//		} 
//	}
//
//	// Update is called once per frame
//	void Update () {
//
//	}
//
//	public void DisplayGUI(){
//		GUI.SetActive (true);
//
//	}
//	public void addToItemList (GameObject item){
//
//	}
}
