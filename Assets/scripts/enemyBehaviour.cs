﻿using UnityEngine;
using System.Collections;

public class enemyBehaviour : MonoBehaviour {

	public float speed;
	public float detectionRadius;
	public int damageValue;
	public int takeDamageValueRange;
	public int takeDamageValueMelee;

	private Vector3 closestPlayerPos;
	private Vector3 playerPos;
	private Vector3 dir;
	private float distance;
	private GameObject nearest;

	private float range;
	private float timer;

	private GameObject[] players;

	private GameObject playerRange;
	private GameObject playerMelee;


	// Use this for initialization
	void Start () {
		players = GameObject.FindGameObjectsWithTag ("player");
		playerRange = GameObject.Find ("player_range");
		playerMelee = GameObject.Find ("player_melee");

		//		Debug.Log (players[0].name + players[1].name);
		distance = Mathf.Infinity;
		range = detectionRadius;
		timer = 0f;
	}
	
	// Update is called once per frame
	void Update ()
	{
		//playerRange = GameObject.Find ("player_range");
		//playerMelee = GameObject.Find ("player_melee");
		//players [0] = playerRange;
		//players [1] = playerMelee;

		//range = detectionRadius;
		//players = GameObject.FindGameObjectsWithTag ("player");


		if (players.Length != 0)
		{
			nearest = null;
			foreach (GameObject player in players)
			{
				if (nearest == null)
				{
					nearest = player;
					distance = Mathf.Abs((transform.position - player.transform.position).magnitude);
				}
				else
				{
					playerPos = player.transform.position;
					float playerDistance = Mathf.Abs((transform.position - player.transform.position).magnitude);
					float nearestDistance = Mathf.Abs ((transform.position - nearest.transform.position).magnitude);
					if (playerDistance < nearestDistance)
					{
						nearest = player;
						distance = playerDistance;
					}
				}
			}
			//dir = (this.gameObject.transform.position - nearest).normalized;

		}

		Debug.Log (distance + "    " + detectionRadius);
		if (distance <= detectionRadius)
		{
			transform.position = Vector3.MoveTowards (transform.position, nearest.transform.position, speed * Time.deltaTime);
		}
//			Debug.Log (nearest);

	}

	IEnumerator attackDelay(GameObject hit){
		yield return new WaitForSeconds (1f);
		attack(hit);
	}

	private void attack(GameObject hit){
		HealthLocal health = hit.GetComponent<HealthLocal> ();
		if (health != null) {
			health.TakeDamage (1);
		}
	}

	void OnTriggerEnter2D(Collider2D col){
		GameObject hit = col.gameObject;
		if (hit.tag == "spell") {
			
			enemyHealth health = gameObject.GetComponent<enemyHealth> ();
			if (health != null) {
				health.TakeDamage (takeDamageValueRange);
			}
			enemyHealthLocal healthLocal = gameObject.GetComponent<enemyHealthLocal> ();
			if (healthLocal != null) {
				healthLocal.TakeDamage (takeDamageValueRange);
			}
		}
		if (hit.tag == "axe") {

			enemyHealth health = gameObject.GetComponent<enemyHealth> ();
			if (health != null) {
				health.TakeDamage (takeDamageValueMelee);
			}
			enemyHealthLocal healthLocal = gameObject.GetComponent<enemyHealthLocal> ();
			if (healthLocal != null) {
				healthLocal.TakeDamage (takeDamageValueMelee);
			}
		}
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.name == "player_melee" || collision.gameObject.name == "player_range") {
			collision.gameObject.GetComponent<Renderer> ().material.color = Color.red;
			float timerE = 0f;
			timerE += Time.deltaTime;
			if (timerE >= 0.1f) {
				collision.gameObject.GetComponent<Renderer> ().material.color = Color.white;
				timerE = 0f;
			}
			GameObject hit = collision.gameObject;
			HealthLocal health = hit.GetComponent<HealthLocal> ();
			if (health != null) {
				health.TakeDamage (damageValue);
			}

			//attackDelay(hit);
			range = -10f;
		}
	}

	void OnCollisionStay2D(Collision2D collision)
	{
		if (collision.gameObject.name == "player_melee" || collision.gameObject.name == "player_range") {
			collision.gameObject.GetComponent<Renderer> ().material.color = Color.red;
			timer += Time.deltaTime;
			GameObject hit = collision.gameObject;
			if (timer >= 0.1f) {
				collision.gameObject.GetComponent<Renderer> ().material.color = Color.white;
			}

			if (timer >= 0.5f) {
				HealthLocal health = hit.GetComponent<HealthLocal> ();
				if (health != null) {
					health.TakeDamage (damageValue);
				}

				timer = 0f;
			}
//			IEnumerator Coroutine = attackDelay (hit);
//			StartCoroutine (Coroutine);
//			HealthLocal health = hit.GetComponent<HealthLocal> ();
//			if (health != null) {
//				health.TakeDamage (10);
//			}
			range = -10f;
		}
	}
		
	void OnCollisionExit2D(Collision2D coll) 
	{
		if (coll.gameObject.name == "player_range" || coll.gameObject.name == "player_melee") 
		{
			coll.gameObject.GetComponent<Renderer> ().material.color = Color.white;
			range = detectionRadius;
		}

	}


}